#include "helpers.hpp"
#include <cinttypes>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <gmpxx.h>
#include <iostream>

using namespace std;

mpz_class factorial(mpz_class n)
{
    mpz_class ret_val = 1u;

    while (n)
    {
        ret_val *= n--;
    }

    return ret_val;
}

int main(void)
{
    cout << factorial(40) / (factorial(20) * factorial(20)) << endl;
}
