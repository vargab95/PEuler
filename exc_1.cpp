#include <iostream>

using namespace std;

int main(void)
{
    unsigned int sum = 0u;

    for (unsigned int i = 1u; i < 1000u; i++)
    {
        if ((0u == i % 3u) || (0u == i % 5u))
            sum += i;
    }

    cout << sum << endl;
}
