#include "helpers.hpp"
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <iostream>

using namespace std;

int main(void)
{
    uint32_t a, b, c, result;

    for (a = 1u; a < 1000u; ++a)
    {
        for (b = (2u < a) ? (a + 1u) : (2u); b < 1000u; ++b)
        {
            for (c = (3u < b) ? (b + 2u) : (3u); c < 1000u; ++c)
            {
                if ((((a * a) + (b * b)) == (c * c)) && (a + b + c == 1000u))
                {
                    cout << a << ' ' << b << ' ' << c << ' ' << a * b * c << ' ' << sqrt((a * a) + (b * b)) << ' '
                         << (uint32_t)sqrt((a * a) + (b * b)) << endl;
                    exit(0);
                }
            }
        }
    }
}
