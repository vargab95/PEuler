#include "helpers.hpp"
#include <cmath>

unsigned int nThFibonacciNumber(unsigned int n)
{
    return (unsigned int)floor(
        ((pow((double)1.6180339, (double)n) - pow((double)-0.6180339, (double)n)) / (double)2.236067977) + 1);
}
