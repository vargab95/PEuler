#ifndef __HELPERS_HPP__
#define __HELPERS_HPP__

#include <cmath>
#include <cstdint>

typedef __int128 int128_t;
typedef unsigned __int128 uint128_t;

unsigned int nThFibonacciNumber(unsigned int n);

#define NTH_PRIME_LOWER_LIMIT(N) ((N * log(N)) + (N * (log(log(N)) - 1)))
#define NTH_PRIME_UPPER_LIMIT(N) ((N * log(N)) + (N * (log(log(N)))))

#endif
