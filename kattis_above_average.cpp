#include "helpers.hpp"
#include <cinttypes>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <gmpxx.h>
#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(void)
{
    unsigned int num_of_test_cases, num_of_students, sum_score, average, num_of_above_average;
    unsigned int *input_arr;

    cin >> num_of_test_cases;

    for (unsigned int test_case_index = 0u; test_case_index < num_of_test_cases; ++test_case_index)
    {
        num_of_above_average = average = sum_score = 0u;

        cin >> num_of_students;

        input_arr = new unsigned int[num_of_students];

        for (unsigned int actual_result = 0u; actual_result < num_of_students; ++actual_result)
        {
            cin >> input_arr[actual_result];
            sum_score += input_arr[actual_result];
        }

        average = sum_score / num_of_students;

        for (unsigned int actual_result = 0u; actual_result < num_of_students; ++actual_result)
        {
            if (average < input_arr[actual_result])
            {
                ++num_of_above_average;
            }
        }

        printf("%.3f%%\n", ((float_t)num_of_above_average / num_of_students) * 100.0);

        delete input_arr;
    }
}
