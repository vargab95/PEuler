#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    uint64_t result, rev, a = 999, b = 999, max = 0;

    while (1)
    {
        rev = 0;
        for (int i = result = a * b; i > 0; i /= 10)
            rev = rev * 10 + i % 10;
        if ((rev == result) && (result > max))
            max = result;

        if (((max / a) < b) && (b > 99))
        {
            --b;
        }
        else if (a > 99)
        {
            b = --a;
        }
        else
        {
            break;
        }
    }

    printf("%d\t%d\t%d\t%d\n", max, result, a, b);
}
