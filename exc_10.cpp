#include "helpers.hpp"
#include <cinttypes>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <iostream>

using namespace std;

int main(void)
{
    uint64_t sum = 0u;
    uint64_t act_num, i;
    uint8_t isPrime = 1u;

    for (act_num = 2u; act_num < 2000000u; act_num++)
    {
        isPrime = 1u;

        for (i = 2; i <= act_num / 2; ++i)
        {
            if (act_num % i == 0)
            {
                isPrime = 0u;
                break;
            }
        }

        if (isPrime)
        {
            sum += act_num;
        }

        if (act_num % 10000u == 0u)
        {
            cout << act_num / 2000000.0f * 100 << " %" << endl;
        }
    }

    cout << sum << endl;
}
