#include "helpers.hpp"
#include <cstdint>
#include <iostream>

using namespace std;

int main(void)
{
    uint64_t i, a, al = (uint64_t)NTH_PRIME_LOWER_LIMIT(10001), au = (uint64_t)NTH_PRIME_UPPER_LIMIT(10001);
    uint8_t isPrime;
    uint16_t prime_cnt = 0;

    for (a = 2u; a < au; a++)
    {
        isPrime = 1u;

        for (i = 2; i <= a / 2; ++i)
        {
            if (a % i == 0)
            {
                isPrime = 0u;
                break;
            }
        }

        if (isPrime)
        {
            ++prime_cnt;

            if (prime_cnt == 10001u)
            {
                cout << a << endl;
                return 0;
            }
        }
    }
}
