#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    unsigned int result = 60u;
    uint8_t all_divable;

    while (1)
    {
        all_divable = 1u;

        for (unsigned int mod_cnt = 7u; mod_cnt <= 20u; mod_cnt++)
        {
            if ((result % mod_cnt) != 0u)
            {
                all_divable = 0u;
            }
        }

        if (all_divable)
        {
            printf("%d\n", result);
            break;
        }

        result += 60u;
    }
}
