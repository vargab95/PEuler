#include "helpers.hpp"
#include <cinttypes>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <iostream>

using namespace std;

int main(void)
{
    uint64_t triangle_num = 0u, last_tn = 0u;
    unsigned int act_num_id = 0u, divisor_cnt;

    do
    {
        unsigned int upper_limit = (triangle_num / 2u) + 1u;

        divisor_cnt = 1u;
        while (upper_limit)
        {
            if (triangle_num % upper_limit == 0u)
            {
                ++divisor_cnt;
            }

            --upper_limit;
        }

        triangle_num += ++act_num_id;

        if (triangle_num < last_tn)
        {
            cout << "!!! OVERFLOW !!!" << endl;
            exit(0);
        }
        else if (act_num_id % 1000u == 0u)
        {
            cout << act_num_id << '\t' << triangle_num << endl;
        }

        last_tn = triangle_num;
    } while (divisor_cnt < 500u);

    cout << triangle_num - act_num_id << endl;
    cout << triangle_num << act_num_id << endl;
}
