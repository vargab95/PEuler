#include "helpers.hpp"
#include <cstdint>
#include <iostream>

using namespace std;

int main(void)
{
    uint_fast64_t sum = 0u, a = 0u, b = 1u, c = 0u;

    uint32_t cnt = 2u;

    while (c < 4000000u)
    {
        c = a + b;
        a = b;
        b = c;

        if (0u == c % 2u)
        {
            sum += c;
        }

        cout << c << ' ' << nThFibonacciNumber(cnt) << endl;
        cnt++;
    }

    cout << sum << endl;
}
