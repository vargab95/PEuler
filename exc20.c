#include "gmp.h"
#include <stdint.h>
#include <stdio.h>

int main(void)
{
    char output[10000] = {0u};

    mpz_t n;
    mpz_init(n);
    mpz_set_ui(n, 1);

    mpz_t looper;
    mpz_init(looper);
    mpz_set_ui(looper, 1);

    mpz_t one;
    mpz_init(one);
    mpz_set_ui(one, 1);

    for (; mpz_get_ui(looper) <= 100u; mpz_add(looper, looper, one))
    {
        mpz_mul(n, n, looper);
    }

    gmp_printf("%Zd \n", n);

    mpz_get_str(output, 10, n);

    fprintf(stdout, "%s\n", output);

    uint32_t foo = 0u;
    uint32_t sum = 0u;
    uint32_t act = 0u;

    while ((act = output[foo++]) != 0)
    {
        sum += act - '0';
    }

    fprintf(stdout, "%d\n", sum);
}
