#include "helpers.hpp"
#include <cinttypes>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <gmpxx.h>
#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(void)
{
    map<unsigned int, string> lut;
    unsigned int looper = 1u, remainder;
    string output = "";

    lut[1] = "one";
    lut[2] = "two";
    lut[3] = "three";
    lut[4] = "four";
    lut[5] = "five";
    lut[6] = "six";
    lut[7] = "seven";
    lut[8] = "eight";
    lut[9] = "nine";
    lut[10] = "ten";
    lut[11] = "eleven";
    lut[12] = "twelve";
    lut[13] = "thirteen";
    lut[14] = "fourteen";
    lut[15] = "fifteen";
    lut[16] = "sixteen";
    lut[17] = "seventeen";
    lut[18] = "eightteen";
    lut[19] = "nineteen";
    lut[20] = "twenty";
    lut[30] = "thirty";
    lut[40] = "fourty";
    lut[50] = "fifty";
    lut[60] = "sixty";
    lut[70] = "seventy";
    lut[80] = "eighty";
    lut[90] = "ninety";
    lut[100] = "hundred";
    lut[1000] = "onethousand";

    while (looper <= 1000u)
    {
        remainder = looper;

        if (remainder / 100u && looper < 1000)
        {
            if (remainder % 100u)
            {
                cout << lut[remainder / 100u] << " hundred and ";
                output += lut[remainder / 100u];
                output += "hundredand";
            }
            else
            {
                cout << lut[remainder / 100u] << " hundred";
                output += lut[remainder / 100u];
                output += "hundred";
            }

            remainder -= (remainder / 100u) * 100u;
        }

        if (remainder > 19u)
        {
            if (remainder / 10u)
            {
                cout << ' ' << lut[(remainder / 10u) * 10u];
                output += lut[(remainder / 10u) * 10u];
            }

            remainder -= (remainder / 10u) * 10u;
        }

        if (remainder <= 19)
        {
            cout << ' ' << lut[remainder];
            output += lut[(remainder / 10u) * 10u];
        }

        cout << endl;
        ++looper;
    }

    cout << output << ' ' << output.size() << endl;
}
