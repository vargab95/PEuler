#include "helpers.hpp"
#include <cinttypes>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <gmpxx.h>
#include <iostream>

using namespace std;

int main(void)
{
    unsigned int act_num = 2u, chain_len, temp, max_ch_len = 0u, max_num = 0u;

    while (act_num < 1000000u)
    {
        chain_len = 0u;

        temp = act_num;
        while (temp != 1u)
        {
            if (temp % 2u)
            {
                temp = 3u * temp + 1;
            }
            else
            {
                temp /= 2u;
            }
            ++chain_len;
        }

        if (act_num % 1000 == 0u)
            cout << act_num << endl;

        if (chain_len > max_ch_len)
        {
            max_ch_len = chain_len;
            max_num = act_num;
        }

        ++act_num;
    }

    cout << max_ch_len << ' ' << max_num << endl;
}
