#include "helpers.hpp"
#include <cinttypes>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <gmpxx.h>
#include <iostream>

using namespace std;

int main(void)
{
    mpz_class r, divider = 10u;
    mpz_ui_pow_ui(r.get_mpz_t(), 2u, 1000u);

    mpz_class res = r % divider;

    while (r /= divider)
    {
        res += r % divider;
    }

    cout << res << endl;
}
