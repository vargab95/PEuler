#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    uint64_t squared_sum = 1u;
    unsigned int sum = 1u;

    for (unsigned int i = 2u; i <= 100u; i++)
    {
        squared_sum += i * i;
        sum += i;
    }

    printf("%d\t%d\t%d\n", squared_sum, (sum * sum), (sum * sum) - squared_sum);
}
